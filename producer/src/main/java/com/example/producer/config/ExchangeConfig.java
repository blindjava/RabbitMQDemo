package com.example.producer.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExchangeConfig {
    /**
     durable="true" 持久化 rabbitmq重启的时候不需要创建新的队列  默认true
     auto-delete 表示消息队列没有在使用时将被自动删除 默认是false
     */
 @Bean
 DirectExchange myDirectExchange(){
     return new DirectExchange("myDirectExchange",true,false);
 }

    @Bean
    FanoutExchange myFanoutExchange(){

        return new FanoutExchange("myFanoutExchange");
    }
    @Bean
    TopicExchange myTopicExchange(){

        return new TopicExchange("myTopicExchange");
    }


}
