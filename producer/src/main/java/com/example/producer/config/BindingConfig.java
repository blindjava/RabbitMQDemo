package com.example.producer.config;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class BindingConfig {
    @Autowired
    QueueConfig queueConfig;
    @Autowired
    ExchangeConfig exchangeConfig;

    /**
     * bind绑定
     *
     * @param destName     目标名称（可以是队列 也可以是交换机）
     * @param type         绑定的类型 交换机 / 队列
     * @param exchangeName 交换机的名称
     * @param routingKey   路由键
     * @param map          结构参数
     * @return
     */
    @Bean
    Binding myBinding() {
        /**
         * arguments键	值	意义
         * x-message-ttl	数字类型，标志时间，以豪秒为单位	标志队列中的消息存活时间，也就是说队列中的消息超过了制定时间会被删除
         * x-expires	数字类型，标志时间，以豪秒为单位	队列自身的空闲存活时间，当前的queue在指定的时间内，没有consumer、basic.get也就是未被访问，就会被删除。
         * x-max-length和x-max-length-bytes	数字	最大长度和最大占用空间，设置了最大长度的队列，在超过了最大长度后进行插入会删除之前插入的消息为本次的留出空间,相应的最大占用大小也是这个道理，当超过了这个大小的时候，会删除之前插入的消息为本次的留出空间。
         * x-dead-letter-exchange和x-dead-letter-routing-key	字符串	消息因为超时或超过限制在队列里消失，这样我们就丢失了一些消息，也许里面就有一些是我们做需要获知的。而rabbitmq的死信功能则为我们带来了解决方案。设置了dead letter exchange与dead letter routingkey（要么都设定，要么都不设定）那些因为超时或超出限制而被删除的消息会被推动到我们设置的exchange中，再根据routingkey推到queue中
         * x-max-priority	数字	队列所支持的优先级别，列如设置为5，表示队列支持0到5六个优先级别，5最高，0最低，当然这需要生产者在发送消息时指定消息的优先级别，消息按照优先级别从高到低的顺序分发给消费者
         * alternate-exchange		下面简称AE，当一个消息不能被route的时候，如果exchange设定了AE，则消息会被投递到AE。如果存在AE链，则会按此继续投递，直到消息被route或AE链结束或遇到已经尝试route过消息的AE。
         */
       Map<String,Object> arguments = new HashMap<String,Object>();
       new Binding(queueConfig.directQueue().getName(), Binding.DestinationType.QUEUE,exchangeConfig.myDirectExchange().getName(),"myDirectQueue",arguments);
      //  BindingBuilder.bind(queueConfig.directQueue()).to(exchangeConfig.myDirectExchange()).with("myDirectQueue");
        return null;
    }

}
