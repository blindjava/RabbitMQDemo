package com.example.producer.aspect;


import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Component
@Target({ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Myannotation {
    /**
     * 操作描述
     */
    String desc() default "66";

    /**
     * 操作类型
     */
    String type() default "77";
}