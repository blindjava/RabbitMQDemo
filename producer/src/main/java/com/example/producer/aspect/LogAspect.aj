package com.example.producer.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.concurrent.ThreadPoolExecutor;

@Aspect
@Component
public class LogAspect {


    @Pointcut("@annotation(com.example.producer.aspect.Myannotation))")
    private void pointCut(){
        System.out.println("切点执行！！！！！！！！！");
    }
//            @Around(value = "@annotation(com.example.producer.aspect.Myannotation)")
//            public void doAfter(ProceedingJoinPoint joinPoint) throws Throwable {
//                Myannotation operate = null;
//                String targetName = joinPoint.getTarget().getClass().getName();
//                String methodName = joinPoint.getSignature().getName();
//                Class targetClass = Class.forName(targetName);
//                java.lang.reflect.Method[] methods = targetClass.getMethods();
//                for (Method method : methods) {
//                    if (method.getName().equals(methodName) && method.isAnnotationPresent(Myannotation.class)) {
//                        operate = method.getAnnotation(Myannotation.class);
//                    }
//                }
//                //获取方法参数值数组
//                Object[] args = joinPoint.getArgs();
//                Object object = joinPoint.proceed(args);
//                System.out.println("保存操作记录:"+operate.type()+"--"+operate.desc());
//
//            //    System.out.println("切面启动---------存储日志");
//
//            }

//    /**
//     * 配置切入点。此切入点是局部切入点，只能在当前切面中引用
//     */
//    @Pointcut("execution(* com.chy.mall.service.UserService.*(..))")
//    private void pointCut(){}
//
//
    /**
     * 前置通知
     * 切入点均可现配  @Before("execution(* com.chy.mall.service.UserService.*(..))")
     */
    @Before("pointCut()")
    public void before(){
        System.out.println("正在执行前置通知..."

        );
    }
//
//
//    /**
//     * 后置通知
//     */
//    @After("pointCut()")
//    public void after(){
//        System.out.println("正在执行后置通知...");
//    }
//
//
//    /**
//     * 返回通知。可将目标方法的返回值传给指定形参
//     */
//    @AfterReturning(value = "pointCut()",returning = "obj" )
//    public void afterReturning(Object obj){
//        System.out.println("正在执行返回通知...");
//        System.out.println("目标方法的返回值是："+obj);
//    }
//
//    /**
//     * 异常通知。可将目标方法的抛出的异常传给指定形参
//     */
//    @AfterThrowing(value = "pointCut()",throwing = "e")
//    public void afterThrowing(JoinPoint point,Exception e){
//        System.out.println("异常信息："+e.getMessage());
//    }

}

