package com.example.producer.controller;

import com.example.producer.service.ProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.rabbitmq.client.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.beans.beancontext.BeanContext;
import java.lang.reflect.Method;

@RestController
@RequestMapping("")
public class ProducerController {
    private final static String QUEUE_NAME = "QUEUE";
    @Autowired
    public   ProducerService producerService;

    //public ProducerController(ProducerService producerService) {
    //    this.producerService = producerService;
    //}

    public ProducerController() {

    }

    //默认交换机
    @GetMapping("hello")
    private String hello() throws JsonProcessingException, InterruptedException {
            producerService.hello();

        return "hello";
    }

    @GetMapping("direct")
    private String direct() throws JsonProcessingException, InterruptedException {
        producerService.directMessage();
        return "direct";
    }


    @GetMapping("fanout")
    private String fanout() throws JsonProcessingException, InterruptedException {
        producerService.fanoutMessage();
        return "fanoutExchange";
    }

    @GetMapping("topic")
    private String topic() throws JsonProcessingException, InterruptedException {
        producerService.topicMessage();
        return "topic";
    }

    @GetMapping("live")
    private String live() throws JsonProcessingException, InterruptedException {
        producerService.liveMessage();
        return "live";
    }
}
