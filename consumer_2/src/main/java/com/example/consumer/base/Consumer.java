package com.example.consumer.base;

import com.alibaba.fastjson.JSON;
import com.example.bean.User;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;


@Component
public class Consumer {
    //@RabbitHandler //接受完全信息
    @RabbitListener(queues = "helloQueue")
    public void hello(@Payload String user) {
        User user1 = JSON.parseObject(user, User.class);

        System.out.println("helloQueue消费者2--------------名称=" + user1.getName() + "-------年龄=" + user1.getAge());
    }

    @RabbitListener(bindings = @QueueBinding(value = @Queue("myDirectQueue2"),exchange =@Exchange(value = "myDirectExchange",type = ExchangeTypes.DIRECT),key = "mind.direct") )
    //@param msg 消息内容,当只有一个参数的时候可以不加@Payload注解
    public void directConsumer( String user ) {
        User user1 = JSON.parseObject(user,User.class);
        System.out.println("directConsumer消费者2--------------名称=" + user1.getName()+"-------年龄="+user1.getAge());
    }

    @RabbitListeners({@RabbitListener(bindings = @QueueBinding(value = @Queue("myFanoutQueue3"), exchange = @Exchange(value = "myFanoutExchange", type = ExchangeTypes.FANOUT), key = "")),
            @RabbitListener(bindings = @QueueBinding(value = @Queue("myFanoutQueue4"), exchange = @Exchange(value = "myFanoutExchange", type = ExchangeTypes.FANOUT), key = ""))})
    public void fanoutConsumer(@Payload String user) {
        User user1 = JSON.parseObject(user, User.class);
        System.out.println("fanoutConsumer消费者2--------------名称=" + user1.getName() + "-------年龄=" + user1.getAge());
    }
}
