package com.example.bean;

import java.io.Serializable;

public class User implements Serializable {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //    private static volatile  User user ;
    public User() {

    }
//     public static User getInstance(){
//        if (user == null){
//         synchronized (User.class){
//             if (user == null){
//                 return new  User();
//             }
//         }
//     }
//        return user;
//     }
}
